"""
r02__variable_str
    s='1'
    s=input('prompt')
    s.zfill(2)

r02__variable_listofstr
    l=s.split('/')
    l[0] l[1]
    
    after_joined_str = '/'.join(a_list)
    
r02__variable_dict
    d={}
    d={'key':'value'}
    d={'key' : 'value', 
       'key2': 'value2' }
    d={
        'key' : 'value', 
        'key2': 'value2',
    }
    d['key'] d['key2']
    d.get('key')
    d.get('key notexist')  # will get None

r02__None_val_w_dictget
    v = d.get('key notexist')
    if v is None:
    if not v:

r02__None_val_w_default_param <- __method_w_default_param
    def f(p):
    f(p='22/12')
    f('22/12')
    f()  # will fail

    def f(p=None):
        if p is None: 
    f()  # will pass

__method_w_default_param
    def runcode():
    runcode()
    
    def rank(y):  # y aka years_as_working_exp
        return 'senior' if y>=3 else 'junior'
    rank(1)
    rank(3)
    rank()  # will fail w/ error eg param y missing
"""
